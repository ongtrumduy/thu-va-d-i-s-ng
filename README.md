#### THUẾ VÀ ĐỜI SỐNG

Link tham khảo: https://www.youtube.com/watch?v=9fj_GpKFtm4

#### Đăng ký tài khoản

Đăng ký tài khoản trên eTax không được?
Đăng ký tài khoản tại Trang Thuế Việt Nam?
--> có cần lên cơ quan thuế để xác minh lại không?

#### Kê khai trực tuyến

Nếu quyết toán thuế TNCN cho sau năm 2020
--> chọn 02/QTT\_\_\_(TT80/2021)

#### Tích chọn khai báo

Tích vào ô này nếu người thuế có duy nhất 01 nguồn
--->

Tích vào ô này nếu người nộp thuế trong năm có từ 02 nguồn thu trở lên
---> trực tiếp khai thuế cho cơ quan thuế --> là phải nộp tờ khai trực tiếp cho cơ quan Thuế --> không thông qua tổ chức nào mà cá nhân trực tiếp
---> lại có cả nguồn thu do tổ chức trả thu nhập khấu trừ

Tích vào ô này nếu người nộp thuế không trực tiếp khai thuế trong năm chỉ có nguồn thu nhập thuộc diện khấu trừ qua tổ chức trả thu nhập
---> trường hợp này là phổ biến nhất
---> có nhiều nguồn thu nhập và mỗi nơi trả thu nhập đó đã khấu trừ thuế của bạn

#### Tích vào ô người nộp thuế không trực tiếp khai thuế trong năm chỉ nguồn thu nhập thuộc diện khấu trừ

Người nộp thuế tích vào ô này nếu có thay đổi nơi làm việc
---> Người nộp thuế có thay đổi nơi làm việc?

Người nộp thuế tích vào ô này nếu không thay đổi nơi làm việc
---> Người nộp thuế không thay đổi nơi làm việc?

#### Người nộp thuế có thay đổi nơi làm việc

Tại thời điểm quyết toán người nộp thuế đang tính giảm trừ gia cảnh cho bản thân tại tổ chức chi trả
---> Nếu vẫn đang làm cho đơn vị nào đó thì tích vào đây
---> Nhập mã số thuế của công ty hiện tại đang làm

Tại thời điểm quyết toán người nộp thuế không làm việc cho tổ chức chi trả .... tại bất kỳ tổ chức chi trả nào
---> Nếu đang thất nghiệp thì sẽ tích vào đây
---> sẽ phải lựa chọn Tỉnh/Thành phố và Quận/Huyện

#### Trường hợp quyết toán thuế

Quyết toán theo năm dương lịch
---> Cá nhân cư trú là người Việt Nam hoặc người nước ngoài đã qua cái năm đầu tiên rồi

Quyết toán 12 tháng liên tục khác năm dương lịch
--->

Quyết toán không đủ 12 tháng
--->

#### Loại tờ khai

Tờ khai chính thức
---> Nếu bạn nộp hồ sơ lần đầu

Tờ khai bổ sung
---> Tờ khai chính thức đã được cơ quan thuế tiếp nhận
---> Thông báo cho chúng ta một số thiếu sót trong quá trình làm tờ khai
---> Thì lúc này mới chọn bổ sung

#### Nhập dữ liệu tờ khai

Chọn Quận/Huyện

Thống kê Thu nhập/Kê khai

Nếu có số thuế đề nghị hoàn --> nhập vào ô thứ [46]

Ô [16] --> Tổng thu nhập chịu thuế đã trả

Cần: Chứng từ khấu trừ Thuế hoặc Thư xác nhận thu nhập
---> Bảo hiểm và các khoản khấu trừ khác
---> khai bảo hiểm vào mục 29

Tổng hợp tất cả [17] để đưa vào [34]

#### Cá nhân có Người phụ thuộc

Tích vào phụ lục 02-1/BK-QTT-TNCN (ở bên dưới cùng)
---> Sang Bảng kê giảm trừ gia cảnh cho Người phụ thuộc

Kê khai --> Quan trọng thời gian tính giảm trừ từ tháng nào đến tháng nào --> Nếu mà từ rất lâu rồi thì sẽ là từ t1->t12
---> Nếu mà mới thì từ tháng nào đến tháng nào điền vào

#### Tổng số thuế đề nghị hoàn trả

Số thuế hoàn trả
---> muốn lấy tiền về xài luôn nhập vào để lấy về

Số thuế bù trừ
---> ví dụ mua cái xe máy có lệ phí trước bạ --> chưa nộp muốn bù trừ luôn vào đó
---> Điền vào phần Khoản nợ, khoản thu phát sinh đề nghị được bù trừ

Ô [48] lúc này sẽ tự động trừ hết đi luôn

#### Số tiền hoàn trả

Nhập thông tin thẻ ngân hàng

#### Kết xuất XML

Tải phần mềm HTKK để đọc hồ sơ thuế XML - iTaxViewer

#### Đính kèm phụ lục

Chứng từ khấu trừ Thuế
---> Do cơ quan chi trả thu nhập trả

Chứng từ nộp thuế
---> Do đã nộp trực tiếp trong năm giờ có

#### Nhập mã OTP

Ấn tiếp tục ---> Nộp tờ khai thành công!

#### Kết quả

Sẽ có tin nhắn gửi về là chờ trong 1 ngày

Nếu mà có tin nhắn CQT chấp nhận hồ sơ khai thuế điện tử mẫu 02
---> Số đề nghị hoàn --> tối đa 40 ngày
---> Số phải nộp thêm

#### Thông báo phản hồi kết quả

Nộp tờ khai thành công!
Bạn đã nộp tờ khai thành công tới CQT
Cơ quan thuế tiếp nhận hồ sơ quyết toán thuế của người nộp thuế là Chi cục Thuế Quận Nam Từ Liêm; Nộp thuế đến KBNN Nam Từ Liêm; chương 757; tiểu mục 1001

Ho so khai thue dien tu mau 02/QTT-TNCN da duoc gui thanh cong, CQT tra Thong bao chap nhan/khong chap nhan sau 01 ngay lam viec. Ma HSDVC 019.01.18.G12-240411-21015500038629

CQT chap nhan ho so khai thue dien tu mau 02/QTT-TNCN; MGD: 11020240012224484; MST: 8368827433; Ky: 2023; So thue phai nop them: 556020 .
